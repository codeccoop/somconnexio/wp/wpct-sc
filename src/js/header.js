function headerNavigation() {
  const liMenu = document.getElementsByClassName(
    " wp-block-navigation-item wp-block-navigation-link has-no-child"
  );

  const liWithChildren = document.getElementsByClassName(
    "wp-block-navigation-item has-child open-on-hover-click wp-block-navigation-submenu"
  );
  Object.keys(liWithChildren).forEach((key) => {
    liWithChildren[key].addEventListener("click", (event) => {
      if ((event.target = liWithChildren[key])) {
        liWithChildren[key].children[0].classList.toggle("is-clicked");
        liWithChildren[key].children[1].classList.toggle("is-toggled");
        liWithChildren[key].children[2].classList.toggle("is-open-submenu");
      }
    });
  });
  Object.keys(liMenu).forEach((key) => {
    liMenu[key].addEventListener("click", (event) => {
      if ((event.target = liMenu[key])) {
        liMenu[key].children[0].classList.toggle("is-clicked");
      }
    });
  });
}

export default headerNavigation;
