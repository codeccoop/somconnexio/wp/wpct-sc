function teamMasonry() {
  var container = document.getElementById("team-members");
  if (!container) {
    return;
  }

  container.addEventListener("waf:render", (ev) => {
    ev.target.style.setProperty("max-height", "unset");
    const children = Array.from(document.querySelectorAll(".sc-team-card"));

    const factor =
      window.innerWidth > 1024 ? 3 : window.innerWidth > 782 ? 2 : 1;
    let maxHeight = 0;
    if (factor === 1) {
      maxHeight = "unset";
    } else {
      const avgHeight =
        children.reduce((acum, el) => acum + el.offsetHeight + 60, 0) /
        children.length;
      const rows = Math.ceil(children.length / factor);
      maxHeight = avgHeight * rows;
    }

    let columns = 1;
    let height = 0;
    children.forEach((child) => {
      if (height + child.offsetHeight + 60 > maxHeight) {
        columns++;
        height = 0;
      }

      height += child.offsetHeight;
    });

    if (columns > factor) {
      maxHeight += height / factor;
    }

    if (factor === 1) {
      ev.target.style.setProperty("max-height", maxHeight);
    } else {
      var maxHeightString = maxHeight + "px";
      ev.target.style.setProperty("max-height", maxHeightString);
    }
  });
}

export default teamMasonry;
