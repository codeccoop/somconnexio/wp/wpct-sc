import teamMasonry from "./team";
import headerNavigation from "./header";

document.addEventListener("DOMContentLoaded", () => {
  teamMasonry();
  headerNavigation();
});
