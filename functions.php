<?php

// Custom blocks
require_once 'custom-blocks/leaflet/leaflet.php';
require_once 'custom-blocks/accordion/accordion.php';
require_once 'custom-blocks/accordion-section/accordion-section.php';
require_once 'custom-blocks/slider/slider.php';
require_once 'custom-blocks/slider-loop/slider-loop.php';
require_once 'custom-blocks/lottie-block/lottie-block.php';
require_once 'custom-blocks/slider-row/slider-row.php';
require_once 'custom-blocks/custom-tags/custom-tags.php';

//cpt

require_once 'includes/cpt/team.php';
require_once 'includes/cpt/testimonial.php';
require_once 'includes/cpt/job_offer.php';

//taxonomies

require_once 'includes/taxonomies/job_offer_state.php';
require_once 'includes/taxonomies/team_taxonomy.php';
require_once 'includes/taxonomies/team_position.php';
//shortcodes
require_once 'includes/shortcodes/breadcrumbs.php';

//waf templates
require_once 'includes/waf.php';

require_once 'includes/template-functions.php';

/**
 * Enqueue Child theme assets.
 */
add_action('wp_enqueue_scripts', 'wpct_enqueue_scripts', 11);
function wpct_enqueue_scripts()
{
    $theme = wp_get_theme();
    $parent = $theme->parent();

    wp_enqueue_style(
        $parent->get_stylesheet(),
        $parent->get_stylesheet_directory_uri() . '/style.css',
        [],
        $parent->get('Version')
    );

    wp_enqueue_style(
        $theme->get_stylesheet(),
        $theme->get_stylesheet_directory_uri() . '/style.css',
        [$parent->get_stylesheet()],
        $theme->get('Version')
    );

    wp_enqueue_script(
        $theme->get_stylesheet(),
        $theme->get_stylesheet_directory_uri() . '/assets/js/theme.bundle.js',
        [$parent->get_stylesheet()],
        $theme->get('Version')
    );
}

add_filter('wpct_gfonts', 'wpct_gfonts');
function wpct_gfonts($fonts)
{
    $fonts = [
        'Work+Sans:ital,wght@0,100..900;1,100..900&display=swap',
        'Gloria+Hallelujah&family=Work+Sans:ital,wght@0,100..900;1,100..900&display=swap',
    ];
    return $fonts;
}

add_action('after_setup_theme', 'wpct_add_theme_support');
function wpct_add_theme_support()
{
    // Add editor-style.css
    add_editor_style('editor-style.css');
}

add_filter('wpct_block_styles', 'wpct_block_styles');
function wpct_block_styles($block_styles)
{
    $block_styles['core/button'] = [
        'small' => __('Small', 'wpct-sc'),
        'outlined' => __('Outline', 'wpct-sc'),
        'underlined' => __('Underlined', 'wpct-sc'),
        'small-outlined' => __('Small Outlined', 'wpct-sc'),
        'small-underlined' => __('Small Underlined', 'wpct-sc'),
    ];

    return $block_styles;
}

add_filter('wpct_pattern_categories', 'wpct_pattern_categories');
function wpct_pattern_categories($pattern_categories)
{
    // Filter coop theme's custom pattern categories
    return $pattern_categories;
}

add_filter(
    'wp_theme_json_data_user',
    function ($theme) {
        // setcookie('sc-wp-theme', 'dark', time() + 86400, COOKIEPATH, COOKIE_DOMAIN);

        $palette = isset($_COOKIE['sc-wp-theme'])
            ? $_COOKIE['sc-wp-theme']
            : 'light';
        if ($palette === 'dark') {
            $json = file_get_contents(dirname(__FILE__) . '/theme-dark.json');
            $dark_config = json_decode($json, true);
            $theme->update_with($dark_config);
        }

        return $theme;
    },
    99
);

// Disable comments
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);
add_filter('comments_array', '__return_empty_array', 20, 2);

add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});

add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});

// Analytics
add_action('wp_head', function () {
    $url = get_option('siteurl');
    $host = parse_url($url)['host'] ?? '';
    ?>
<!-- Plausible Analytics -->
<script defer data-domain="<?php echo $host; ?>" src="https://web-analytics-sc.coopdevs.org/js/script.manual.js"></script>
<script>window.plausible=window.plausible||function(){(window.plausible.q=window.plausible.q||[]).push(arguments);};</script>
<script>window.plausible("pageview",{props:{pathname:window.location.pathname,hash:window.location.hash}});</script>
<!-- End Plausible Analytics -->
<?php
});

add_action('wp_footer', function () {
    ?>
<!-- GForm Leads -->
<script>
document.addEventListener('gform/theme/scripts_loaded', () => {
    gform.utils.addAsyncFilter('gform/ajax/post_ajax_submission', async (data) => {
        if (!data.submissionResult.success) return data;
        if (!data.submissionResult.data.is_valid) return data;

        if (typeof cmplz_has_consent === "function" && cmplz_has_consent('statistics')) {
            const formId = +data.form.dataset.formid;
            window.dataLayer.push({
                event: "Event Lead",
                isCompany: formId === 3 || formId === 4,
            });
        }

        return data;
    });
});
</script>
<!-- End GForm Leads -->
<?php
});

// SMTP
add_action('phpmailer_init', function ($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = SC_SMTP_HOST;
    $phpmailer->Port = SC_SMTP_PORT;
    $phpmailer->SMTPSecure = SC_SMTP_SECURE;
    $phpmailer->SMTPAuth = true;
    $phpmailer->Username = SC_SMTP_USERNAME;
    $phpmailer->Password = SC_SMTP_PASSWORD;
    $phpmailer->From = SC_SMTP_USERNAME;
    $phpmailer->FromName = 'Som Connexio | Staging';
    $phpmailer->addReplyTo(SC_SMTP_USERNAME, 'Som Connexió');
});

add_action('wp_mail_failed', function ($error) {
    error_log('# WP Email Error');
    error_log(print_r($error, true));
});

/** Forms Bridge */
add_filter('gform_form_args', function ($args) {
    $args['submission_method'] = GFFormDisplay::SUBMISSION_METHOD_AJAX;
    return $args;
});

add_filter(
    'gform_field_value_referral',
    function () {
        $refurl = isset($_SERVER['HTTP_REFERER'])
            ? sanitize_text_field(wp_unslash($_SERVER['HTTP_REFERER']))
            : '';
        return esc_url_raw($refurl);
    },
    10,
    0
);

add_filter(
    'forms_bridge_payload',
    function ($payload) {
        if (
            isset($payload['DynamicField']) &&
            is_array($payload['DynamicField'])
        ) {
            $dynamic_fields = [];
            foreach ($payload['DynamicField'] as $field => $value) {
                $dynamic_fields[] = ['Name' => $field, 'Value' => $value];
            }

            $payload['DynamicField'] = $dynamic_fields;
        }

        return $payload;
    },
    10,
    1
);
