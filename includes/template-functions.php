<?php

add_shortcode('sharing_buttons', 'wpct_sc_social_share_buttons_script');
function wpct_sc_social_share_buttons_script($post)
{
    $post = get_post($post);
    $post_id = $post->ID;
    $title = rawurlencode(get_the_title($post_id));
    $post_url = rawurlencode(get_post_permalink($post_id));

    $share_text = rawurlencode(
        sprintf(
            __(
                'Acabo de leer este artículo, si quieres acceder visita: ',
                'wpct-sc'
            ),
            $title
        )
    );
    $tweet_text = rawurlencode(
        sprintf(
            __(
                'Acabo de leer este artículo, si quieres acceder visita: ',
                'wpct-ce'
            ),
            $title
        )
    );

    ob_start();
    ?>
    <div>
        <script>
            window.addEventListener("load", (event) => {

                const ceSharePannels = document.getElementsByClassName("sc-share-pannel");
                for (const pannel of ceSharePannels) {
                    var ceShareBtns = pannel.querySelectorAll(".wp-social-link");
                    for (var btn of ceShareBtns) {
                        var link = btn.children[0];
                        if (btn.classList.contains("sc-share__telegram")) {
                            link.href = "https://telegram.me/share/url?url=<?= $post_url ?>&text=<?= $share_text ?>";
                        } else if (btn.classList.contains("sc-share__whatsapp")) {
                            link.href = "https://api.whatsapp.com/send?text=<?= $share_text ?>+<?= $post_url ?>";
                        } else if (btn.classList.contains("sc-share__facebook")) {
                            link.href = "http://www.facebook.com/sharer/sharer.php?u=<?= $post_url ?>&t=<?= $title ?>";
                        } else if (btn.classList.contains("sc-share__linkedin")) {
                            link.href = "https://www.linkedin.com/sharing/share-offsite/?url=<?= $post_url ?>";
                        } else if (btn.classList.contains("sc-share__mastodon")) {
                            link.href = "https://mastodonshare.com/?text=<?= $tweet_text ?>&url=<?= $post_url ?>";
                        }
                    }
                }

            });
        </script>
    </div>
<?php
$buffer = ob_get_clean();
return str_replace(["\r", "\n"], '', $buffer);
}
