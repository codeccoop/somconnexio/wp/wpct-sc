<?php

add_action('init', 'wpct_register_job_offer_state_taxonomy', 99);
function wpct_register_job_offer_state_taxonomy()
{
    $labels = [
        'name' => __("Estats de l'oferta", 'wpct-sc'),
        'singular_name' => __("Estat de l'oferta", 'wpct-sc'),
    ];

    $args = [
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'has_archive' => false,
        'rewrite' => ['slug' => 'Estat Oferta'],
        'hierarchical' => false,
    ];
    register_taxonomy('job_offer_state', 'job_offer', $args);
}
