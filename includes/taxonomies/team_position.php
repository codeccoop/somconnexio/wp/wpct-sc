<?php
add_action('init', 'wpct_register_position_taxonomy', 99);
function wpct_register_position_taxonomy()
{
    $labels = [
        'name' => __('Càrrec de la persona', 'wpct-sc'),
        'singular_name' => __('Càrrec de la persona', 'wpct-sc'),
    ];

    $args = [
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'has_archive' => false,
        'rewrite' => ['slug' => 'Carrec de la persona'],
        'hierarchical' => false,
    ];
    register_taxonomy('position_taxonomy', 'team', $args);
}
