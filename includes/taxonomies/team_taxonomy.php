<?php

add_action('init', 'wpct_register_equip_taxonomy', 99);
function wpct_register_equip_taxonomy()
{
    $labels = [
        'name' => __("Taxonomia de l'equip", 'wpct-sc'),
        'singular_name' => __("Taxonomia de l'equip", 'wpct-sc'),
    ];

    $args = [
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'has_archive' => false,
        'rewrite' => ['slug' => 'Taxonomia equip'],
        'hierarchical' => false,
    ];
    register_taxonomy('team_taxonomy', 'team', $args);
}
