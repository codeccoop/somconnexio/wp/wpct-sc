<?php

function sc_get_breadcrumbs()
{
    $page = get_post();
    if ($page->post_type !== 'page') {
        return '';
    }
    $page_parent = get_post($page->post_parent);
    if (!$page_parent) {
        return '';
    }
    $breadcrumbs = [$page, $page_parent];
    while ($page_parent) {
        $page_parent = $page_parent->post_parent
            ? get_post($page_parent->post_parent)
            : null;
        if ($page_parent) {
            $breadcrumbs[] = $page_parent;
        }
    }
    $html = '';
    $breadcrumbs_reverse = array_reverse($breadcrumbs);
    foreach ($breadcrumbs_reverse as $breadcrumb) {
        $html .=
            '<li class="sc-breadcrumbs-item">' .
            $breadcrumb->post_title .
            '</li>';
    }
    return '<ul class="sc-breadcrumbs">' . $html . '</ul>';
}
add_shortcode('sc_breadcrumbs', 'sc_get_breadcrumbs');
