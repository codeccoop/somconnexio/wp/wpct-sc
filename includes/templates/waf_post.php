<?php
$post = get_post($post_id);
$taxonomies = get_post_taxonomies($post);
?>
<div data-post="<?= $post_id ?>" class="sc-post-card waf-post waf-entry wp-block-group is-nowrap">
    <?= get_the_post_thumbnail($post_id, 'post-thumbnail') ?>
    <h4 class="wp-block-post-title">
        <?= $post->post_title ?>
    </h4>
    <p class="post-date"><?= get_the_date('M, Y', $post_id) ?></p>
    <div class="sc-link-container">
        <a class="sc-link sc-post-show-more" href="<?= get_post_permalink(
            $post_id
        ) ?>"><?= __('Leer más', 'wpct-sc') ?></a>
    </div>
</div>