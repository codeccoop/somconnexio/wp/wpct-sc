<?php
$post = get_post($post_id);
$position = get_the_terms($post_id, 'position_taxonomy');
$position = isset($position[0]) ? $position[0]->name : '';
?>
<div data-post="<?= $post_id ?>" class="sc-team-card waf-team waf-entry wp-block-group is-nowrap">
    <?= get_the_post_thumbnail($post_id, 'post-thumbnail') ?>
    <p class="sc-team-position">
        <?php echo $position; ?>
    </p>
    <h4 class="wp-block-team-title">
        <?php echo $post->post_title; ?>
    </h4>
    <div class="wp-block-team-excerpt has-text-color">
        <p class="wp-block-team-excerpt__excerpt"><?php echo get_the_excerpt(); ?></p>
    </div>
    <div class="wp-block-team-content has-text-color">
        <p class="wp-block-team-content__content"><?php echo get_the_content(
            $post_id
        ); ?></p>
    </div>
</div>
