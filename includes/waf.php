<?php
// Intercepts service template for waf archives

// Intercepts service template for waf archives
add_filter(
    'waf_template_team',
    function ($html, $post_id) {
        ob_start();
        include get_stylesheet_directory() . '/includes/templates/waf_team.php';
        return ob_get_clean();
    },
    20,
    2
);
add_filter(
    'waf_template_post',
    function ($html, $post_id) {
        ob_start();
        include get_stylesheet_directory() . '/includes/templates/waf_post.php';
        return ob_get_clean();
    },
    20,
    2
);
