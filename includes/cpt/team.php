<?php
add_action('init', 'wpct_create_model_team');
// add_action('init', 'insert_default_category_term');
function wpct_create_model_team()
{
    register_post_type('team', [
        'labels' => [
            'name' => __('Equip', 'wpct-sc'),
            'singular_name' => __('Equip', 'wpct-ce'),
        ],

        // Frontend
        'has_archive' => true,
        'public' => true,
        'publicly_queryable' => true,

        // Admin
        'capability_type' => 'post',
        'menu_icon' => 'dashicons-admin-home',
        'menu_position' => 28,
        'query_var' => true,
        'show_in_menu' => true,
        'show_ui' => true,
        'show_in_rest' => true,
        'supports' => ['title', 'thumbnail', 'excerpt', 'author', 'editor'],
        'rewrite' => [
            'slug' => 'equip',
        ],
        'taxonomies' => ['team_taxonomy'],
    ]);
}
