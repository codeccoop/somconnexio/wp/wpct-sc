<?php
add_action('init', 'wpct_create_model_job_offer');
// add_action('init', 'insert_default_category_term');
function wpct_create_model_job_offer()
{
    register_post_type('job_offer', [
        'labels' => [
            'name' => __('Ofertes', 'wpct-sc'),
            'singular_name' => __('Oferta', 'wpct-ce'),
        ],

        // Frontend
        'has_archive' => true,
        'public' => true,
        'publicly_queryable' => true,

        // Admin
        'capability_type' => 'post',
        'menu_icon' => 'dashicons-admin-home',
        'menu_position' => 29,
        'query_var' => true,
        'show_in_menu' => true,
        'show_ui' => true,
        'show_in_rest' => true,
        'supports' => ['title', 'thumbnail', 'excerpt', 'author', 'editor'],
        'rewrite' => [
            'slug' => 'ofertes',
        ],
        'taxonomies' => ['post_tag', 'job_offer_state'],
    ]);
}
