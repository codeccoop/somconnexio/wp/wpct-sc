<?php
add_action('init', 'wpct_create_model_testimonial');
// add_action('init', 'insert_default_category_term');
function wpct_create_model_testimonial()
{
    register_post_type('testimonial', [
        'labels' => [
            'name' => __('Testimoni', 'wpct-sc'),
            'singular_name' => __('Testimoni', 'wpct-ce'),
        ],

        // Frontend
        'has_archive' => true,
        'public' => true,
        'publicly_queryable' => true,

        // Admin
        'capability_type' => 'post',
        'menu_icon' => 'dashicons-admin-home',
        'menu_position' => 30,
        'query_var' => true,
        'show_in_menu' => true,
        'show_ui' => true,
        'show_in_rest' => true,
        'supports' => ['title', 'thumbnail', 'excerpt', 'author', 'editor'],
        'rewrite' => [
            'slug' => 'testimoni',
        ],
        'taxonomies' => [],
    ]);
}
