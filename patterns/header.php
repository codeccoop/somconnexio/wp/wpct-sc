<?php

/**
 * Title: SC Header
 * Slug: wpct/header
 * Categories: wpct-pattern
 * Viewport Width: 1500
 */

?>

<!-- wp:group {"align":"full","backgroundColor":"brand","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull is-style-horizontal-padded has-brand-background-color has-background"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var(\u002d\u002dwp\u002d\u002dcustom\u002d\u002dspacing\u002d\u002dsm)","bottom":"var(\u002d\u002dwp\u002d\u002dcustom\u002d\u002dspacing\u002d\u002dsm)"},"margin":{"top":"0px","bottom":"0"}}},"className":"is-style-horizontal-padded","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
<div class="wp-block-group is-style-horizontal-padded" style="margin-top:0px;margin-bottom:0;padding-top:var(--wp--custom--spacing--sm);padding-bottom:var(--wp--custom--spacing--sm)"><!-- wp:image {"id":4854,"width":"163px","height":"40px","scale":"contain","sizeSlug":"full","linkDestination":"custom","style":{"layout":{"selfStretch":"fixed","flexSize":"163px"}},"className":"is-style-default"} -->
<figure class="wp-block-image size-full is-resized is-style-default"><img src="https://somconnexio.local/wp-content/uploads/2017/06/somconnexio_noulogo.png" alt="" class="wp-image-4854" style="object-fit:contain;width:163px;height:40px"/></figure>
<!-- /wp:image -->

<!-- wp:navigation {"ref":21163,"textColor":"typography","overlayMenu":"never","overlayBackgroundColor":"main","overlayTextColor":"white","className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right"},"style":{"layout":{"selfStretch":"fit","flexSize":null},"typography":{"textTransform":"none"}}} /-->

<!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-show-desktop","layout":{"type":"default"}} -->
<div class="wp-block-group is-style-show-desktop has-base-color has-text-color has-link-color"></div>
<!-- /wp:group -->

<!-- wp:paragraph -->
<p><a href="https://wordpress.com/home/somit.coop"></a></p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom"} -->
<figure class="wp-block-image"><a href="/"><img src="https://somit.coop/wp-content/uploads/2024/01/favicon4_somit.png" alt="Site Icon"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
