import { __ } from "@wordpress/i18n";
import {
	useBlockProps,
	InnerBlocks,
	InspectorControls,
} from "@wordpress/block-editor";
import { PanelBody } from "@wordpress/components";
import PaletteControl from "./PaletteControl.js";
import LinkControl from "./LinkControl.js";
import TeaserImage from "./TeaserImage.js";
import "./editor.scss";

export default function Edit({ attributes, setAttributes }) {
	const { image, href, cta, background } = attributes;

	const setAttribute = (attr, value) => {
		setAttributes({ [attr]: value });
	};

	return (
		<>
			<InspectorControls>
				<PanelBody title={__("Teaser settings", "wpct")}>
					<PaletteControl
						value={background}
						setValue={(value) => setAttribute("background", value)}
					/>
					<LinkControl cta={cta} href={href} setAttributes={setAttributes} />
				</PanelBody>
			</InspectorControls>
			<figure {...useBlockProps()} style={{ background }}>
				<TeaserImage
					src={image}
					onChange={(value) => setAttribute("image", value)}
				/>
				<figcaption>
					<InnerBlocks template={TEMPLATE} templateLock="all" />
					{href && cta && (
						<p className="wpct-teaser-cta">
							<a className="wpct-teaser-link" href={href}>
								{cta}
							</a>
						</p>
					)}
				</figcaption>
			</figure>
		</>
	);
}

const TEMPLATE = [
	[
		"core/group",
		{
			layout: {
				type: "flex",
				orientation: "vertical",
			},
			style: {
				spacing: {
					blockGap: "0",
					padding: { top: "0", bottom: "0" },
					margin: { top: "0", bottom: "0" },
				},
			},
			className: "wpct-teaser-content",
			templateLock: false,
		},
		[
			[
				"core/heading",
				{ level: 3, placeholder: __("Teaser title", "wpct") },
			],
			["core/paragraph", { placeholder: __("Teaser text", "wpct") }],
		],
	],
];
