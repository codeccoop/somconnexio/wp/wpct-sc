import { __ } from "@wordpress/i18n";
import {
	BlockControls,
	MediaPlaceholder,
	MediaReplaceFlow,
} from "@wordpress/block-editor";
import { useState } from "@wordpress/element";

export default function TeaserImage({ src, onChange }) {
	const [imageSrc, setImageSrc] = useState(src);
	const onSelect = (el) => {
		onChange(el.url);
		setImageSrc(el.url);
	};

	if (imageSrc) {
		return (
			<>
				<BlockControls group="other">
					<MediaReplaceFlow
						mediaURL={src}
						allowedTypes={["image"]}
						accept="image/*"
						onSelect={onSelect}
					/>
				</BlockControls>
				<img clasName="wpct-teaser-thumbnail" src={src} />
			</>
		);
	}

	return (
		<MediaPlaceholder
			onSelect={(el) => onSelect(el)}
			allowedTypes={["image"]}
			multiple={false}
			labels={{ title: __("Teaser image", "wpct") }}
		/>
	);
}
