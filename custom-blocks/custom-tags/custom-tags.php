<?php
/**
 * Plugin Name:       Custom tags
 * Description:       Display custom tags with custom template.
 * Requires at least: 6.1
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            The WordPress Contributors
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       wpct-sc
 *
 * @package           wpct-sc-custom-tags
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */
add_action('init', 'wpct_sc_custom_tags_block_init');
function wpct_sc_custom_tags_block_init()
{
    register_block_type(__DIR__ . '/build');
}
