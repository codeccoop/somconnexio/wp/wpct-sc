/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/
 */
import { __ } from "@wordpress/i18n";

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#useblockprops
 */

import { useBlockProps } from "@wordpress/block-editor";
import { useEntityProp, store as coreStore } from "@wordpress/core-data";
import { useSelect } from "@wordpress/data";

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import "./editor.scss";

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#edit
 *
 * @return {Element} Element to render.
 */
export default function Edit({ context: { postId, postType } }) {
  const terms = useSelect((select) => {
    const { getTaxonomy } = select(coreStore);
    return {
      status: getTaxonomy("job_offer_state"),
    };
  }, []);

  const { status } = useSelect(
    (select) => {
      if (!terms.status) {
        return { status: null };
      }
      const { getEntityRecords, isResolving } = select(coreStore);

      const getTerms = (term) => {
        const taxonomyArgs = [
          "taxonomy",
          term.slug,
          {
            post: postId,
            per_page: -1,
            context: "view",
          },
        ];

        const terms = getEntityRecords(...taxonomyArgs);

        return {
          terms,
          isLoading: isResolving("getEntityRecords", taxonomyArgs),
          has: !!terms?.length,
        };
      };

      return {
        status: getTerms(terms.status),
      };
    },
    [postId, terms]
  );
  const blockProps = useBlockProps();
  console.log(status);
  if (status.isLoading || !status.terms || !postId || postType !== "job_offer")
    return null;

  return (
    <div {...blockProps}>
      {status.terms.map((term) => {
        console.log(term.name);
        return (
          <div className="taxonomy-rest-ce-status wp-block-post-terms">
            <a rel="tag" tabindex="0">
              {term.name}
            </a>
          </div>
        );
      })}
    </div>
  );
}
