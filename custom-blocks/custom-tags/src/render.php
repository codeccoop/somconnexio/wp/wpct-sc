<?php

/**
 * @see https://github.com/WordPress/gutenberg/blob/trunk/docs/reference-guides/block-api/block-metadata.md#render
 */

//  $attributes (array): The block attributes.
//  $content (string): The block default content.
//  $block (WP_Block): The block instance.

$post_ID = get_the_ID();

$post_type = get_post_type($post_ID);
if ($post_type === 'job_offer') {
    $terms = get_the_terms($post_ID, 'job_offer_state');
} else {
    $terms = get_the_terms($post_ID, 'post_tag');
}
if (!$terms) {
    echo '';
}
foreach ($terms as $term) { ?>

    <div class="sc-custom-tag-<?= $term->slug ?> taxonomy-post_tag">
        <a href=''><?= $term->name ?></a>

    </div>
<?php }
