<?php

add_action('init', 'wpct_block_accordion_init');
function wpct_block_accordion_init()
{
    register_block_type(__DIR__ . '/build');
}

add_action('after_setup_theme', 'wpct_block_accordion_scripts');
function wpct_block_accordion_scripts()
{
    $base_url = get_stylesheet_directory_uri();
    wp_enqueue_block_style('wpct-block/accordion', [
        'handle' => 'jquery-ui',
        'src' =>
            $base_url .
            '/custom-blocks/accordion/assets/jquery-ui/jquery-ui.min.css',
        'deps' => [],
        'ver' => '1.14.0',
        'media' => 'all',
    ]);
}
add_filter('render_block', 'wpct_enqueue_accordion_assets', 10, 2);
function wpct_enqueue_accordion_assets($block_content, $block)
{
    // Check if the specific block is being rendered
    $base_url = get_stylesheet_directory_uri();
    if ('wpct-block/accordion' === $block['blockName']) {
        // Enqueue your script or style here
        wp_enqueue_script(
            'jquery-ui',
            $base_url .
                '/custom-blocks/accordion/assets/jquery-ui/jquery-ui.min.js',
            ['jquery'],
            '1.14.0'
        );
    }
    return $block_content;
}
