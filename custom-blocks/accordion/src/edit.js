import { __ } from "@wordpress/i18n";
import {
  useBlockProps,
  InspectorControls,
  store as blockEditorStore,
  InnerBlocks,
} from "@wordpress/block-editor";
import { useSelect } from "@wordpress/data";
import {
  PanelBody,
  ToggleControl,
  SelectControl,
  BaseControl,
  useBaseControlProps,
} from "@wordpress/components";
import "./editor.scss";

const MOUSE_EVENTS = [
  { label: __("On click", "wpct"), value: "click" },
  { label: __("On mouse over"), value: "mouseover" },
];

const BEHVAIOURS = [
  { label: __("Fit tallest", "wpct"), value: "auto" },
  { label: __("Fill parent", "wpct"), value: "fill" },
  { label: __("Fit content", "wpct"), value: "content" },
];

function AccordionInspectorControls({ clientId, attributes, setAttributes }) {
  const {
    active = 0,
    animate = true,
    collapsible = true,
    event = "click",
    height = "content",
  } = attributes;

  const { getBlocks } = useSelect(blockEditorStore);
  const innerBlocks = getBlocks(clientId);

  const { baseControlProps, controlProps } = useBaseControlProps({
    min: 0,
    max: innerBlocks.length - 1,
    label: __("Active section", "wpct"),
    value: active,
    required: true,
    onChange: (value) => setAttributes({ active: value }),
  });

  return (
    <InspectorControls>
      <PanelBody title={__("Accordion settings", "wpct")}>
        <BaseControl {...baseControlProps}>
          <input type="number" {...controlProps} />
        </BaseControl>
        <SelectControl
          label={__("Event", "wpct")}
          value={event}
          onChange={(value) => setAttributes({ event: value })}
          options={MOUSE_EVENTS}
          required
        />
        <SelectControl
          label={__("Height behaviour", "wpct")}
          value={height}
          onChange={(value) => setAttributes({ height: value })}
          options={BEHVAIOURS}
          required
        />
        <ToggleControl
          label={__("Animated", "wpct")}
          checked={animate}
          onChange={() => setAttributes({ animate: !animate })}
        />
        <ToggleControl
          label={__("Collapsible", "wpct")}
          checked={collapsible}
          onChange={() => setAttributes({ collapsible: !collapsible })}
        />
      </PanelBody>
    </InspectorControls>
  );
}

export default function Edit({ clientId, attributes, setAttributes }) {
  const blockProps = useBlockProps({
    className: "wpct-block-accordion",
    layout: { type: "constrained" },
  });

  const template = Array.from(Array(attributes.sections)).map(() => [
    "wpct-block/accordion-section",
  ]);

  return (
    <>
      <AccordionInspectorControls
        attributes={attributes}
        setAttributes={setAttributes}
        clientId={clientId}
      />
      <div {...blockProps}>
        <InnerBlocks
          templateLock={false}
          orientation="vertical"
          directInsert={true}
          template={template}
        />
      </div>
    </>
  );
}
