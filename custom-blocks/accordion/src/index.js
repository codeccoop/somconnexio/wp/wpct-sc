import { registerBlockType } from "@wordpress/blocks";

import "./style.scss";
// import "./editor.scss";

import Edit from "./edit";
import save from "./save";
import metadata from "./block.json";

const icon = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="24"
    height="24"
    context="list-view"
    aria-hidden="true"
    focusable="false"
  >
    <g>
      <path d="M0,0v6h24V0H0z M22,4H2V2h20V4z"></path>
    </g>
    <g>
      <path d="M0,18v6h24v-6H0z M22,22H2v-2h20V22z"></path>
    </g>
    <g>
      <path d="M0,8v8h24V8H0z M22,14H2v-4h20V14z"></path>
    </g>
  </svg>
);

registerBlockType(metadata.name, {
  edit: Edit,
  save,
  icon,
});
