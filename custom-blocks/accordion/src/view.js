(function () {
  document.addEventListener("DOMContentLoaded", function () {
    document.querySelectorAll(".wpct-block-accordion").forEach((el) => {
      el.classList.add("ready");
      const { active, animate, collapsible, event, height } = getAttributes(el);

      const sections = el.querySelectorAll(".wpct-block-accordion-section");
      sections.forEach((section) => {
        Array.from(section.children).forEach((child) => {
          el.insertBefore(child, section);
        });
        el.removeChild(section);
      });

      jQuery(el).accordion({
        active,
        animate,
        collapsible,
        event,
        heightStyle: height,
        classes: {
          "ui-accordion-header": "wpct-accordion-header",
          "ui-accordion-header-collapsed": "wpct-accordion-header-collapsed",
          "ui-accordion-content": "wpct-accordion-content",
        },
        header: ".wpct-accordion-header",
      });
    });
  });

  function getAttributes(el) {
    return {
      active: el.dataset.active || 0,
      animate: el.dataset.animate === "true",
      collapsible: el.dataset.collapsible === "true",
      event: el.dataset.event,
      height: el.dataset.height,
    };
  }
})();
