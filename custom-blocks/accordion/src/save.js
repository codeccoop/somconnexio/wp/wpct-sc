import { useBlockProps, InnerBlocks } from "@wordpress/block-editor";

export default function save({ attributes }) {
  const { active, animate, collapsible, event, height } = attributes;

  const blockProps = useBlockProps.save({
    className: "wpct-block-accordion",
    layout: { type: "constrained" },
  });

  return (
    <div
      {...blockProps}
      data-active={active}
      data-animate={String(animate)}
      data-collapsible={String(collapsible)}
      data-event={event}
      data-height={height}
    >
      <InnerBlocks.Content />
    </div>
  );
}
