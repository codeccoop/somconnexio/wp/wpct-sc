<?php

add_action('init', function () {
    register_block_type(__DIR__ . '/build');
});

add_action('wp_enqueue_scripts', function () {
    wp_register_script(
        'leaflet',
        'https://unpkg.com/leaflet@1.9.4/dist/leaflet.js',
        [],
        '1.9.4'
    );
});

add_action('enqueue_block_editor_assets', function () {
    wp_enqueue_script(
        'leaflet',
        'https://unpkg.com/leaflet@1.9.4/dist/leaflet.js',
        [],
        '1.9.4'
    );
});

add_action('after_setup_theme', function () {
    wp_enqueue_block_style('wpct-block/leaflet', [
        'handle' => 'leaflet',
        'src' => 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.css',
        [],
        '1.9.4',
    ]);
});

add_filter(
    'render_block',
    function ($block_content, $block) {
        if ('wpct-block/leaflet' === $block['blockName']) {
            wp_enqueue_script('leaflet');
        }
        return $block_content;
    },
    10,
    2
);
