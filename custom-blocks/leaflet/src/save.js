import { useBlockProps } from "@wordpress/block-editor";

export default function save({ attributes }) {
  const { latitude, longitude, zoom } = attributes;

  const blocksProps = useBlockProps.save({
    className: "wpct-block-leaflet",
  });

  return (
    <div
      {...blocksProps}
      data-latitude={latitude}
      data-longitude={longitude}
      data-zoom={zoom}
    ></div>
  );
}
