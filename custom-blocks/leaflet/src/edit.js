import { __ } from "@wordpress/i18n";
import { InspectorControls, useBlockProps } from "@wordpress/block-editor";
import {
  PanelBody,
  BaseControl,
  useBaseControlProps,
  Button,
  Modal,
} from "@wordpress/components";
import { useState, useEffect, useRef } from "@wordpress/element";

import "./editor.scss";

function Map(el, { zoom, latitude, longitude }, zoomControl = false) {
  const center = window.L.latLng(+latitude, +longitude);
  const map = window.L.map(el, { zoomControl });
  map.setView(center, +zoom);
  window.L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
    maxZoom: 19,
    attribution:
      '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  }).addTo(map);

  return map;
}

function MapModal({ attributes, setAttributes, onClose }) {
  const { latitude, longitude, zoom } = attributes;

  const mapEl = useRef();

  useEffect(() => {
    if (!mapEl.current) return;

    const map = Map(
      mapEl.current,
      {
        zoom,
        latitude,
        longitude,
      },
      true
    );
    map.on("click", ({ latlng }) => {
      setAttributes({
        latitude: latlng.lat,
        longitude: latlng.lng,
        zoom: map.getZoom(),
      });
      onClose();
    });
  }, []);

  return (
    <Modal
      title={__("Set your location", "wpct-child-theme")}
      onRequestClose={onClose}
    >
      <div style={{ height: "300px", width: "100%" }} ref={mapEl}></div>
      <Button variant="secondary" onClick={onClose}>
        {__("Close", "wpct-child-theme")}
      </Button>
    </Modal>
  );
}

function MapInspectorControls({ attributes, setAttributes }) {
  const { zoom = 13 } = attributes;

  const { baseControlProps, controlProps } = useBaseControlProps({
    label: __("Zoom", "wpct-child-theme"),
    value: zoom,
    required: true,
  });

  return (
    <InspectorControls>
      <PanelBody title={__("Slider settings", "wpct-child-theme")}>
        <Button
          variant="primary"
          onClick={() => setAttributes({ interactionMode: true })}
        >
          {__("Set your location", "wpct-child-theme")}
        </Button>
        <BaseControl {...baseControlProps}>
          <input
            value={zoom}
            type="number"
            min="1"
            max="15"
            onChange={({ target }) => setAttributes({ zoom: +target.value })}
            {...controlProps}
          />
        </BaseControl>
      </PanelBody>
    </InspectorControls>
  );
}

export default function Edit({ attributes, setAttributes }) {
  const map = useRef();
  const marker = useRef();
  const mapEl = useRef();

  useEffect(() => {
    if (!mapEl.current) return;

    setTimeout(() => {
      map.current = Map(mapEl.current, attributes);
      map.current._handlers.forEach(function (handler) {
        handler.disable();
      });

      marker.current = window.L.marker([
        attributes.latitude,
        attributes.longitude,
      ]).addTo(map.current);
    }, 500);
  }, []);

  useEffect(() => {
    if (!map.current) return;

    const center = window.L.latLng(+attributes.latitude, +attributes.longitude);
    map.current.setView(center, +attributes.zoom);
    marker.current.setLatLng(center);
  }, [attributes]);

  const blockProps = useBlockProps({
    className: "wpct-block-leaflet",
    layout: { type: "constrained" },
  });

  const [interactionMode, setInteractionMode] = useState(false);

  const inspectorAttributes = { ...attributes, interactionMode };
  const setInspectorAttributes = (newAttributes) => {
    newAttributes = { ...attributes, ...newAttributes };
    setInteractionMode(!!newAttributes.interactionMode);
    delete newAttributes.interactionMode;
    setAttributes(newAttributes);
  };

  return (
    <>
      <MapInspectorControls
        attributes={inspectorAttributes}
        setAttributes={setInspectorAttributes}
      />
      {interactionMode && (
        <MapModal
          attributes={attributes}
          setAttributes={setAttributes}
          onClose={() => setInteractionMode(false)}
        />
      )}
      <div {...blockProps}>
        <div
          ref={mapEl}
          style={{ height: blockProps.style?.minHeight || "300px" }}
        ></div>
      </div>
    </>
  );
}
