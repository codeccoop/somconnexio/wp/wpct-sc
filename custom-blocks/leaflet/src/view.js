(function () {
  document.addEventListener("DOMContentLoaded", function () {
    document.querySelectorAll(".wpct-block-leaflet").forEach((el) => {
      const latitude = +el.dataset.latitude;
      const longitude = +el.dataset.longitude;
      const zoom = +el.dataset.zoom;

      if (!el.style.minHeight) {
        el.style.minHeight = "300px";
      }

      const center = window.L.latLng(latitude, longitude);
      const map = window.L.map(el);
      map.setView(center, zoom);

      window.L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 19,
        attribution:
          '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      }).addTo(map);

      window.L.marker([latitude, longitude]).addTo(map);
    });
  });
})();
