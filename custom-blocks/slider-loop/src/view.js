(function () {
  document.addEventListener("DOMContentLoaded", function () {
    document.querySelectorAll(".wpct-block-slider-loop").forEach((el) => {
      el.classList.add("ready");
      const {
        infinite,
        slidesToShow,
        slidesToScroll,
        initialSlide,
        centerMode,
        dots,
        arrows,
        autoplay,
        rtl,
        autoplaySpeed,
        customPaging,
      } = getAttributes(el);

      const wrapper = document.createElement("div");
      const wrapperMobile = document.createElement("div");
      wrapper.classList.add("wpct-block-slider-wrapper");
      wrapperMobile.classList.add("wpct-block-slider-wrapper-mobile");
      const slides = el.querySelectorAll(".wpct-block-slider-slide");
      slides.forEach((slide) => {
        var cloneSlide = slide.cloneNode(true);
        slide.parentElement.removeChild(slide);
        wrapper.appendChild(slide);
        wrapperMobile.appendChild(cloneSlide);
      });

      el.innerHTML = "";
      el.appendChild(wrapper);
      el.appendChild(wrapperMobile);

      if (rtl) {
        wrapper.setAttribute("dir", "rtl");
        wrapperMobile.setAttribute("dir", "rtl");
      }

      const arrowsWrapper = document.createElement("div");
      const arrowsWrapperMobile = document.createElement("div");
      arrowsWrapper.classList.add("wpct-block-slider-arrows");
      arrowsWrapperMobile.classList.add("wpct-block-slider-arrows-mobile");
      el.appendChild(arrowsWrapper);
      el.appendChild(arrowsWrapperMobile);

      if (customPaging) {
        wrapper.classList.add("with-pagination");
        wrapperMobile.classList.add("with-pagination");
        jQuery(wrapper).slick({
          dots,
          arrows,
          slidesToScroll,
          slidesToShow,
          infinite,
          centerMode,
          autoplay,
          autoplaySpeed,
          initialSlide: Math.max(0, Math.min(slides.length, initialSlide)),
          appendArrows: jQuery(arrowsWrapper),
          // lazyLoad: "ondemand",
          adaptiveHeight: true,
          rtl,
          customPaging: function (slider, i) {
            return i + 1 + "/" + slider.slideCount;
          },
        });
        jQuery(wrapperMobile).slick({
          dots,
          arrows,
          slidesToScroll,
          slidesToShow: 1,
          infinite,
          centerMode,
          autoplay,
          autoplaySpeed,
          initialSlide: Math.max(0, Math.min(slides.length, initialSlide)),
          appendArrows: jQuery(arrowsWrapperMobile),
          // lazyLoad: "ondemand",
          adaptiveHeight: true,
          rtl,
          customPaging: function (slider, i) {
            return i + 1 + "/" + slider.slideCount;
          },
        });
      } else {
        jQuery(wrapper).slick({
          dots,
          arrows,
          slidesToScroll,
          slidesToShow,
          infinite,
          centerMode,
          autoplay,
          autoplaySpeed,
          initialSlide: Math.max(0, Math.min(slides.length, initialSlide)),
          appendArrows: jQuery(arrowsWrapper),
          // lazyLoad: "ondemand",
          adaptiveHeight: true,
          rtl,
        });
        jQuery(wrapperMobile).slick({
          dots,
          arrows,
          slidesToScroll,
          slidesToShow: 1,
          infinite,
          centerMode,
          autoplay,
          autoplaySpeed,
          initialSlide: Math.max(0, Math.min(slides.length, initialSlide)),
          appendArrows: jQuery(arrowsWrapperMobile),
          // lazyLoad: "ondemand",
          adaptiveHeight: true,
          rtl,
        });
      }
    });
  });

  function getAttributes(el) {
    return {
      infinite: el.getAttribute("infinite") === "true",
      centerMode: el.getAttribute("centermode") === "true",
      slidesToShow: Number(el.getAttribute("slidestoshow")),
      slidesToScroll: Number(el.getAttribute("slidestoscroll")),
      initialSlide: Number(el.getAttribute("initialslide")),
      dots: el.getAttribute("showdots") === "true",
      arrows: el.getAttribute("showarrows") === "true",
      autoplay: el.getAttribute("animation") === "true",
      autoplaySpeed: Number(el.getAttribute("animationspeed")) * 1000,
      customPaging: el.getAttribute("customPaging") === "true",
      rtl: el.getAttribute("rtl") === "true",
    };
  }
})();
