
import {useBlockProps, InspectorControls} from '@wordpress/block-editor';
import {DotLottie} from "@lottiefiles/dotlottie-web";
import { useEffect, useRef } from '@wordpress/element';
import BlockControls from './blockControls';



export default function Edit({attributes, setAttributes}) {
  const {
    url, 
    autoplay,
    speed
  } = attributes;
  const canvas = useRef();

  useEffect(()=>{
    if (canvas === null) return;
    new DotLottie ({
      autoplay: autoplay,
      loop: true,
      speed: speed,
      canvas: canvas.current,
      src: url,
    });
  }, [url, autoplay])
 console.log(attributes)
  return (  
    <>
      <BlockControls attributes={attributes} setAttributes={setAttributes}/>
      <canvas {...useBlockProps()} ref={canvas}></canvas>
    </>
   
  )
}