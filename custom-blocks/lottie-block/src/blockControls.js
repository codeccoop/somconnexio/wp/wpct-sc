import { CheckboxControl, PanelBody, RangeControl, TextControl } from '@wordpress/components';
import { InspectorControls } from '@wordpress/block-editor';
import {__} from "@wordpress/i18n";

export default function BlockControls ({attributes, setAttributes}) {
    const {
        url, 
        autoplay,
        speed
      } = attributes;
    function onChangeUrl (value) {
        setAttributes ({url: value})
    }
    function onChangeAutoplay (value) {
    
        setAttributes ({autoplay: value})
    }
    function onChangeSpeed (speed){
        setAttributes ({speed: speed})
    }
    
    return (
        <InspectorControls>
            <PanelBody title={__('Lottie Settings', 'wpct-sc')}>
                <TextControl 
                label={__('Animation URL', 'wpct-sc')}
                value={url}
                onChange={onChangeUrl}
                />
                <CheckboxControl
                label={__('Autoplay', 'wpct-sc')}
                checked= {autoplay}
                onChange={onChangeAutoplay}
                />
                <RangeControl
                label={__("Animation speed", "wpct-sc")}
                value={speed}
                onChange={onChangeSpeed}
                min={0.5}
                max={10}
                step={0.5}
                />

            </PanelBody>
        </InspectorControls>
    )

}