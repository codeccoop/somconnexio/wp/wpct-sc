import { useBlockProps } from "@wordpress/block-editor";


export default function save({attributes}){
  const blockProps = useBlockProps.save({
    className: "wpct-lottie-block"
  })
  const {
    url, 
    autoplay,
    speed
  } = attributes;
  return (
    <canvas {...blockProps} data-url={url} data-autoplay={autoplay} data-speed={speed}></canvas>
  )
}