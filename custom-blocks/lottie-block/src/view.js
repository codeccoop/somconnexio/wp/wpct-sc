import {DotLottie} from "@lottiefiles/dotlottie-web";

(function () {
  document.addEventListener("DOMContentLoaded", function () {
    document.querySelectorAll(".wpct-lottie-block").forEach((el) => {
      const {url, autoplay, speed} = getAttributes(el);
      new DotLottie  ({
        src: url,
        canvas: el,
        autoplay: autoplay,
        loop: true, 
        speed: speed
      })

    });
  });

  function getAttributes(el) {
    return {
      autoplay: el.getAttribute("data-autoplay") === "true",
      url: el.getAttribute("data-url"),
      speed: el.getAttribute("data-speed")
      
     
    };
  }
})();
