(function () {
  document.addEventListener("DOMContentLoaded", function () {
    document.querySelectorAll(".wpct-block-slider").forEach((el) => {
      if (el.classList.contains("wpct-block-slider-loop")) {
        return;
      }

      el.classList.add("ready");
      const {
        infinite,
        slidesToShow,
        slidesToScroll,
        initialSlide,
        centerMode,
        dots,
        arrows,
        autoplay,
        autoplaySpeed,
      } = getAttributes(el);

      const wrapper = document.createElement("div");
      const wrapperMobile = document.createElement("div");
      wrapper.classList.add("wpct-block-slider-wrapper");
      wrapperMobile.classList.add("wpct-block-slider-wrapper-mobile");

      const slides = el.querySelectorAll(".wpct-block-slider-slide");
      slides.forEach((slide) => {
        var cloneSlide = slide.cloneNode(true);
        el.removeChild(slide);
        wrapper.appendChild(slide);
        wrapperMobile.appendChild(cloneSlide);
      });

      el.appendChild(wrapper);
      el.appendChild(wrapperMobile);

      const arrowsWrapper = document.createElement("div");
      const arrowsWrapperMobile = document.createElement("div");
      arrowsWrapper.classList.add("wpct-block-slider-arrows");
      arrowsWrapperMobile.classList.add("wpct-block-slider-arrows-mobile");
      el.appendChild(arrowsWrapper);
      el.appendChild(arrowsWrapperMobile);

      jQuery(wrapper).slick({
        dots,
        arrows,
        slidesToScroll,
        slidesToShow,
        infinite,
        centerMode,
        autoplay,
        autoplaySpeed,
        initialSlide: Math.max(0, Math.min(slides.length, initialSlide)),
        appendArrows: jQuery(arrowsWrapper),
        // lazyLoad: "ondemand",
        adaptiveHeight: true,
      });
      jQuery(wrapperMobile).slick({
        dots,
        arrows,
        slidesToScroll,
        slidesToShow: 1,
        infinite,
        centerMode,
        autoplay,
        autoplaySpeed,
        initialSlide: Math.max(0, Math.min(slides.length, initialSlide)),
        appendArrows: jQuery(arrowsWrapperMobile),
        // lazyLoad: "ondemand",
        adaptiveHeight: true,
      });
    });
  });

  function getAttributes(el) {
    return {
      infinite: el.getAttribute("infinite") === "true",
      centerMode: el.getAttribute("centermode") === "true",
      slidesToShow: Number(el.getAttribute("slidestoshow")),
      slidesToScroll: Number(el.getAttribute("slidestoscroll")),
      initialSlide: Number(el.getAttribute("initialslide")),
      dots: el.getAttribute("showdots") === "true",
      arrows: el.getAttribute("showarrows") === "true",
      autoplay: el.getAttribute("animation") === "true",
      autoplaySpeed: Number(el.getAttribute("animationspeed")) * 1000,
    };
  }
})();
