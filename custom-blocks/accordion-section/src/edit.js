import { __ } from "@wordpress/i18n";
import { useBlockProps, InnerBlocks } from "@wordpress/block-editor";
import "./editor.scss";

export default function Edit() {
  const blockProps = useBlockProps({
    className: "wpct-block-accordion-section",
  });

  console.log({ blockProps });

  return (
    <div {...blockProps}>
      <InnerBlocks
        template={TEMPLATE}
        templateLock={false}
        directInsert={true}
        orientation="vertical"
      />
    </div>
  );
}

const TEMPLATE = [
  [
    "core/group",
    {
      layout: { type: "constrained" },
      metadata: { name: "Accordion title" },
      className: "wpct-accordion-section-header",
      lock: {
        remove: true,
        move: true,
      },
    },
    [
      [
        "core/heading",
        {
          level: 3,
          placeholder: "Title",
          lock: {
            remove: false,
            move: false,
          },
        },
      ],
    ],
  ],
  [
    "core/group",
    {
      layout: { type: "constrained" },
      metadata: { name: "Content" },
      className: "wpct-accordion-section-content",
      lock: {
        remove: true,
        move: true,
      },
    },
    [
      [
        "core/paragraph",
        {
          lock: {
            remove: false,
            move: false,
          },
          placeholder: "Section content",
        },
      ],
    ],
  ],
];
