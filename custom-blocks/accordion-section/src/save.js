import { useBlockProps, InnerBlocks } from "@wordpress/block-editor";

export default function save() {
  const blockProps = useBlockProps.save({
    className: "wpct-block-accordion-section",
    layout: { type: "constrained" },
  });

  return (
    <div {...blockProps}>
      <InnerBlocks.Content />
    </div>
  );
}
