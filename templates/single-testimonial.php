<?php

/**
 * Template Name: Single Testimonial Card
 * Type: sc-single-testimonial
 *
 *
 */

$post_id = get_the_ID();
ob_start();
?>
<a class="wpct-sc_testimonial" href="<?= get_post_permalink(
    $post_id
) ?>" tabindex="0">
  <!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between","verticalAlignment":"top"}} -->
  <div id="testimonial-template" class="wp-block-group">
  
    <!-- wp:group {"layout":{"type":"constrained"}} -->
  
    <div class="wp-block-group">
      <!-- wp:post-title /-->
    
      <!-- wp:post-excerpt /-->
    </div>
    <!-- /wp:group -->
    
    <!-- wp:image {"id":21640,"sizeSlug":"full","linkDestination":"none"} -->
    <figure class="wp-block-image size-full"><img src="https://somconnexio.local/wp-content/uploads/2024/10/Quote-down.png" alt="" class="wp-image-21640"/></figure>
    <!-- /wp:image -->
    </div>
    <!-- /wp:group -->
    
  <!-- wp:post-content /-->
</a>


<?php
$content = do_shortcode(do_blocks(ob_get_clean()));
echo $content;

